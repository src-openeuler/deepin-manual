%bcond_with check
%global debug_package   %{nil}
%global _unpackaged_files_terminate_build 0  
Name:          deepin-manual
Version:       5.8.15
Release:       1
Summary:       Manual is designed to help users learn the operating system and its applications, providing specific instructions and function descriptions.
License:       GPLv3
URL:           https://uos-packages.deepin.com/uos/pool/main/d/deepin-manual/
Source0:       %{name}-%{version}.tar.gz

BuildRequires: cmake
BuildRequires: dtkcore-devel
BuildRequires: dtkwidget-devel
BuildRequires: dtkgui-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: qt5-linguist
BuildRequires: qt5-qtwebchannel
BuildRequires: qt5-qtwebchannel-devel
BuildRequires: qt5-qtwebengine-devel
BuildRequires: qt5-qtwebengine
BuildRequires: gtest-devel
BuildRequires: gmock-devel
BuildRequires: qt5-qtbase-private-devel

Requires: qt5-qtwebengine
Requires: dde-manual-content

# qt5-qtwebengine
Excludearch:    loongarch64

%description
Manual is designed to help users learn the operating system and its applications, providing specific instructions and function descriptions.


%prep
%setup -q

# fix strip
sed -i 's|gc-sections"|gc-sections -s"|' src/CMakeLists.txt

%build
export PATH=$PATH:/usr/lib64/qt5/bin
# cmake_minimum_required version is too high
%cmake
%make_build

%install
%make_install INSTALL_ROOT=%{buildroot}

%files
%{_bindir}/dman
%{_bindir}/dmanHelper
%{_datadir}/applications/deepin-manual.desktop
%{_datadir}/dbus-1/services/com.deepin.Manual.Open.service
%{_datadir}/dbus-1/services/com.deepin.Manual.Search.service
%{_datadir}/%{name}/web_dist/
%{_datadir}/icons/hicolor/scalable/apps/deepin-manual.svg
%{_datadir}/%{name}/translations/*
%license LICENSE
%doc README.md


%changelog
* Wed Jul 26 2023 leeffo <liweiganga@uniontech.com> - 5.8.15-1
- upgrade to version 5.8.15

* Fri Mar 31 2023 liweiganga <liweiganga@uniontech.com> - 5.7.28-1
- update: update to 5.7.28

* Wed Mar 15 2023 liweiganga <liweiganga@uniontech.com> - 5.6.7-8
- feat: fix strip

* Fri Feb 10 2023 liweiganga <liweiganga@uniontech.com> - 5.6.7-7
- feat: fix compile fail

* Thu Sep 3 2020 weidong <weidong@uniontech.com> - 5.6.7-6
- fix source url in spec

* Wed Sep 2 2020 chenbo pan <panchenbo@uniontech.com> - 5.6.7-5
- fix compile fail 

* Fri Aug 28 2020 chenbo pan <panchenbo@uniontech.com> - 5.6.7-4
- fix compile fail

* Thu Jul 30 2020 openEuler Buildteam <buildteam@openeuler.org> - 5.6.7-3
- Package init
